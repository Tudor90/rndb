/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Navigator
} from 'react-native';

var InitialScreen = require('./Custom classes/InitialScreen');
var MovieList = require('./Custom classes/MovieList');

export default class RNDB extends Component {

  render() {
    return (
      <Navigator
      initialRoute = {{
        id:'InitialScreen'
      }}
       renderScene={this.navigatorRenderScene}
      />
    );
  }

  navigatorRenderScene(route,navigator)
  {
    _navigator = navigator;
    switch(route.id)
    {
      case 'InitialScreen':
        return (<InitialScreen navigator={navigator} title ="Welcome" />);
      case 'MovieList':
        return (<MovieList navigator={navigator} title ="MovieList" moviesArray={route.moviesArray} />);
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});

AppRegistry.registerComponent('RNDB', () => RNDB);
