import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  View,
  ListView,
  Text,
  Image,
  TouchableOpacity
} from 'react-native';

var basePictureURL = "http://image.tmdb.org/t/p/w185/";

function extractField(objectsArray, field) {
    var fieldArray = [];
    for (var i=0; i < objectsArray.length ; ++i)
        fieldArray.push(objectsArray[i][field]);
    return fieldArray;
}

class MovieList extends Component {

    constructor(props)
    {
        super(props);
        var movies = this.props.moviesArray["results"];
       // var titlesArray = extractField(this.props.moviesArray["results"], "title");
        var _dataSource = new ListView.DataSource({rowHasChanged:(r1,r2) => r1 != r2});
        this.state = { dataSource : _dataSource.cloneWithRows(movies)};
    }

    displayMovie(movie)
    {
        var moviePosterPath = basePictureURL + movie.poster_path
       // console.log(moviePosterPath)
        return(
        <TouchableOpacity>
            <View style={styles.movieCellContainer}>
                <Image 
                    source = {{uri: moviePosterPath}}
                    style = {styles.posterPicture}
                />
                 <Text>
                     {movie.title}
                  </Text>
                  <View style={styles.separator} />
            </View>
        </TouchableOpacity>
        );
    }

    render()
    {
        return(
        <View style = {styles.container}>
        <ListView
            style = {styles.listViewStyle}
            dataSource = {this.state.dataSource}
            renderRow = {this.displayMovie.bind(this)}
            
           // renderSeparator = { }
        />
        </View>
        );
    }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    paddingTop: 20,
  },
  listViewStyle: {
      paddingTop: 10,
      flex: 1
  },
  separator: {
       height: 1,
       backgroundColor: 'black',
   },
   posterPicture: {
        width: 46,
        height: 70,
        marginRight: 20,
        marginLeft: 20
    },
    movieCellContainer: {
        flex: 1,
        alignItems: 'center',
        flexDirection: 'row',
        paddingTop: 10
    }
});

module.exports = MovieList;
