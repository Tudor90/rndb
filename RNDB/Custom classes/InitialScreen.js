import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Image,
  ActivityIndicator,
  Navigator
} from 'react-native';

var _moviesArray;

function getMovies() {
  fetch("https://api.themoviedb.org/3/movie/popular?api_key=48fbdac127c2866cf1c755f719d025c9&language=en-US&page=1", { method: "GET" })
    .then((response) => response.json())
    .then((responseData) => {
      parseResponse(responseData);
    })
    .done();
}

function parseResponse(response) {
  _moviesArray = response;
}

class InitialScreen extends Component {
  render() {
    getMovies();
    return (
      <View style={styles.container}>
        <Text>{this.props.title}</Text>
        <Image source={require('../Images/imdbLogo.png')} style={{ width: 100, height: 100 }} />
        <Text style={styles.welcome} onPress={ ()=>{this.props.navigator.push({id:"MovieList", moviesArray:_moviesArray})} }>
          Loading movies...
        </Text>
        <ActivityIndicator
          style={[styles.centering, { height: 80 }]}
          size="large"
          />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
    color: '#333333'
  },
  centering: {
    alignItems: 'center',
    justifyContent: 'center',
    padding: 8,
  }
});

module.exports = InitialScreen;